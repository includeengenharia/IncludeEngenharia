﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IncludeEngenharia.ViewModels
{
    public class ServicosViewModel : ViewModelBase
    {
        public ServicosViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Servicos";
        }
    }
}
