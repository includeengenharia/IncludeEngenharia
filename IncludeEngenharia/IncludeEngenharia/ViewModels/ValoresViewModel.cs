﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IncludeEngenharia.ViewModels
{
    public class ValoresViewModel : ViewModelBase
    {
        public ValoresViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Valores da Include";
        }
    }
}
