﻿using System;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using IncludeEngenharia.Models;
using Prism.Services;
using SQLite;
using Xamarin.Forms;

namespace IncludeEngenharia.ViewModels
{
    public class AdicionarContatoViewModel : ViewModelBase
    {
        public IPageDialogService _dialogService { get; set; }

        public DelegateCommand<string> NavigateCommand { get; }

        public DelegateCommand<string> ToolbarItem_Clicked { get; }


        // ----------- Constructor -----------

        public AdicionarContatoViewModel(INavigationService navigationService,
                                          IPageDialogService dialogService) : base(navigationService)
        {
            _dialogService = dialogService;
            NavigateCommand = new DelegateCommand<string>(OnNavigateCommandExecuted);
            ToolbarItem_Clicked = new DelegateCommand<string>(OnToolbarItem_Clicked);

        }


        // ----------- Command Methods ---------

        private async void OnNavigateCommandExecuted(string path)
        {
            await _navigationService.NavigateAsync(path);
        }

        private async void OnToolbarItem_Clicked(string path)
        {
            Contato contato = new Contato()
            {
                Nome_Lead = Nome_LeadEntry,
                Celular_Lead = Celular_LeadEntry,
                Email_Lead = Email_LeadEntry,
                Interesse_Lead = InteresseEntry,

            };

            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Contato>();
                int rows = conn.Insert(contato);

                if (rows > 0)
                {
                    await _dialogService.DisplayAlertAsync("Sucesso", "Contato adicionado com sucesso", "Ok");
                    //await _navigationService.NavigateAsync(path);
                    await _navigationService.GoBackAsync();
                }
                else
                    await _dialogService.DisplayAlertAsync("Falha", "Ocorreu uma falha e o contato não pôde ser adicionada", "Ok");
            }
        }


        // -------Properties -------------

        private string _Nome_LeadEntry;
        public string Nome_LeadEntry
        {
            get { return _Nome_LeadEntry; }
            set
            {
                SetProperty(ref _Nome_LeadEntry, value);
            }
        }
        private string _Celular_LeadEntry;
        public string Celular_LeadEntry
        {
            get { return _Celular_LeadEntry; }
            set { SetProperty(ref _Celular_LeadEntry, value); }
        }

        private string _Email_LeadEntry;
        public string Email_LeadEntry
        {
            get { return _Email_LeadEntry; }
            set { SetProperty(ref _Email_LeadEntry, value); }
        }

        private string _InteresseEntry;
        public string InteresseEntry
        {
            get { return _InteresseEntry; }
            set { SetProperty(ref _InteresseEntry, value); }
        }
    }
}
