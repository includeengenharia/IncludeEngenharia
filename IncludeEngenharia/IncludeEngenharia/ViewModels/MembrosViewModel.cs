﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IncludeEngenharia.ViewModels
{
    public class MembrosViewModel : ViewModelBase
    {
        public MembrosViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Membros";
        }
    }
}

